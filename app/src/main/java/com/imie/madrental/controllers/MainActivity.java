package com.imie.madrental.controllers;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.widget.FrameLayout;

import com.imie.madrental.R;
import com.imie.madrental.data.ApplicationDatabaseHelper;

/**
 * Main activity of the application.
 */
public class MainActivity extends AppCompatActivity {

    /**
     * Stores the vehicle key.
     */
    public static final String VEHICLE_KEY = "vehicle";

    /**
     * Defines if the detail fragment is shown on its own.
     */
    private boolean newFragmentShown = false;

    /**
     * Gets the newFragmentShown attribute.
     * @return The boolean value of the newFragmentShown attribute.
     */
    public boolean isNewFragmentShown() {
        return this.newFragmentShown;
    }

    /**
     * Sets the newFragmentShown attribute.
     * @param newNewFragmentShown The new boolean value to set.
     */
    public void setNewFragmentShown(boolean newNewFragmentShown) {
        this.newFragmentShown = newNewFragmentShown;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ApplicationDatabaseHelper.getDatabase(this);
                
        setContentView(R.layout.activity_main);
    }

    @Override
    public void onBackPressed() {
        if (this.newFragmentShown) {
            this.newFragmentShown = false;
            
            FragmentManager fragmentManager = this.getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            
            fragmentTransaction.remove(fragmentManager.findFragmentByTag(VehicleDetailFragment.TAG));
            
            fragmentTransaction.commit();
        } else {
            super.onBackPressed();
        }
    }
    
}

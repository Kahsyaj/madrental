/*
 * EventSessionListAdapter.java, presence Android
 *
 * Copyright © 2019-2020 Mickael Gaillard / TACTfactory
 * License    : all rights reserved
 */
package com.imie.madrental.controllers.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.imie.madrental.R;
import com.imie.madrental.data.DTO.EquipmentDTO;

import java.util.ArrayList;
import java.util.List;

/**
 * List adapter for equipments.
 */
public class EquipmentListAdapter extends RecyclerView.Adapter<EquipmentListAdapter.EquipmentViewHolder> {

    /**
     * Stores the list of equipments to display.
     */
    private List<EquipmentDTO> equipments = new ArrayList<>();

    /**
     * Constructor.
     * @param equipments the options to set.
     */
    public EquipmentListAdapter(List<EquipmentDTO> equipments) {
        super();

        this.equipments = equipments;
    }

    /**
     * Gets the equipments attribute.
     * @return The java.util.List<com.imie.madrental.data.DTO.EquipmentDTO> value of the equipments attribute.
     */
    public List<EquipmentDTO> getEquipments() {
        return this.equipments;
    }

    /**
     * Sets the equipments attribute.
     * @param newEquipments The new java.util.List<com.imie.madrental.data.DTO.EquipmentDTO> value to set.
     */
    public void setEquipments(List<EquipmentDTO> newEquipments) {
        this.equipments = newEquipments;
    }

    /**
     * Called when RecyclerView needs a new {@link ViewHolder} of the given type to represent
     * an item.
     * <p>
     * This new ViewHolder should be constructed with a new View that can represent the items
     * of the given type. You can either create a new View manually or inflate it from an XML
     * layout file.
     * <p>
     * The new ViewHolder will be used to display items of the adapter using
     * {@link #onBindViewHolder(ViewHolder, int, List)}. Since it will be re-used to display
     * different items in the data set, it is a good idea to cache references to sub views of
     * the View to avoid unnecessary {@link View#findViewById(int)} calls.
     * @param parent The ViewGroup into which the new View will be added after it is bound to
     * an adapter position.
     * @param viewType The view type of the new View.
     * @return A new ViewHolder that holds a View of the given view type.
     * @see #getItemViewType(int)
     * @see #onBindViewHolder(ViewHolder, int)
     */
    @NonNull
    @Override
    public EquipmentViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.equipment_row, parent, false);
        
        return new EquipmentViewHolder(view);
    }

    /**
     * Called by RecyclerView to display the data at the specified position. This method should
     * update the contents of the {@link ViewHolder#itemView} to reflect the item at the given
     * position.
     * <p>
     * Note that unlike {@link ListView}, RecyclerView will not call this method
     * again if the position of the item changes in the data set unless the item itself is
     * invalidated or the new position cannot be determined. For this reason, you should only
     * use the <code>position</code> parameter while acquiring the related data item inside
     * this method and should not keep a copy of it. If you need the position of an item later
     * on (e.g. in a click listener), use {@link ViewHolder#getAdapterPosition()} which will
     * have the updated adapter position.
     * <p>
     * Override {@link #onBindViewHolder(ViewHolder, int, List)} instead if Adapter can
     * handle efficient partial bind.
     * @param holder The ViewHolder which should be updated to represent the contents of the
     * item at the given position in the data set.
     * @param position The position of the item within the adapter's data set.
     */
    @Override
    public void onBindViewHolder(@NonNull EquipmentViewHolder holder, int position) {
        holder.populate(this.equipments.get(position));
    }

    /**
     * Returns the total number of items in the data set held by the adapter.
     * @return The total number of items in this adapter.
     */
    @Override
    public int getItemCount() {
        return this.equipments.size();
    }

    /**
     * Using holder pattern in order to display elements.
     */
    protected class EquipmentViewHolder extends RecyclerView.ViewHolder {

        /**
         * Stores the equipment name text.
         */
        private final TextView txtName;

        /**
         * Constructor.
         * @param view The view.
         */
        protected EquipmentViewHolder(View view) {
            super(view);

            this.txtName = view.findViewById(R.id.txt_name);
        }

        /**
         * Populate row with a equipment.
         * @param model The model to display.
         */
        protected void populate(final EquipmentDTO model) {
            this.txtName.setText(model.name);
        }
    }
    
}

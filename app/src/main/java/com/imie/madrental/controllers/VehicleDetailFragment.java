package com.imie.madrental.controllers;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.imie.madrental.R;
import com.imie.madrental.controllers.adapters.EquipmentListAdapter;
import com.imie.madrental.controllers.adapters.OptionListAdapter;
import com.imie.madrental.data.ApplicationDatabase;
import com.imie.madrental.data.ApplicationDatabaseHelper;
import com.imie.madrental.data.DAO.VehicleDAO;
import com.imie.madrental.data.DTO.VehicleDTO;
import com.imie.madrental.data.mappers.VehicleMapper;
import com.squareup.picasso.Picasso;

import org.w3c.dom.Text;

import java.util.List;

/**
 * Fragment displaying the detail of a vehicle.
 */
public class VehicleDetailFragment extends Fragment {

    /**
     * Stores the associated tag.
     */
    public static final String TAG = "VehicleDetailFragment";

    /**
     * Stores the displayed vehicle.
     */
    private VehicleDTO vehicle;

    /**
     * Stores the vehicle image.
     */
    private ImageView imgVehicle;

    /**
     * Stores the favorite image button.
     */
    private ImageButton imgBtnFavorite;

    /**
     * Stores the displayed id text.
     */
    private TextView txtId;

    /**
     * Stores the displayed name text.
     */
    private TextView txtName;

    /**
     * Stores the displayed available text.
     */
    private TextView txtAvailable;

    /**
     * Stores the displayed price text.
     */
    private TextView txtPrice;

    /**
     * Stores the displayed age min text.
     */
    private TextView txtAgeMin;

    /**
     * Stores the displayed discount text.
     */
    private TextView txtDiscount;

    /**
     * Stores the displayed category text.
     */
    private TextView txtCategory;

    /**
     * Stores the list of equipments.
     */
    private RecyclerView recyclerEquipments;

    /**
     * Stores the list of options.
     */
    private RecyclerView recyclerOptions;

    /**
     * Constructor.
     */
    public VehicleDetailFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        this.vehicle = (VehicleDTO) this.getArguments().getSerializable(MainActivity.VEHICLE_KEY);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_vehicle_detail, container, false);

        this.init(view);
        
        return view;
    }

    /**
     * Initializes the fragment.
     * @param view The fragment's view.
     */
    private void init(View view) {
        this.imgVehicle = view.findViewById(R.id.img_vehicle);
        this.imgBtnFavorite = view.findViewById(R.id.img_btn_favorite);
        this.txtId = view.findViewById(R.id.txt_id);
        this.txtName = view.findViewById(R.id.txt_name);
        this.txtAvailable = view.findViewById(R.id.txt_available);
        this.txtPrice = view.findViewById(R.id.txt_price);
        this.txtAgeMin = view.findViewById(R.id.txt_age_min);
        this.txtDiscount = view.findViewById(R.id.txt_discount);
        this.txtCategory = view.findViewById(R.id.txt_category);

        Picasso.get().load(String.format(VehicleMapper.VEHICLE_IMAGE_URL, this.vehicle.image)).into(this.imgVehicle);
        
        this.txtId.setText(String.valueOf(this.vehicle.id));
        this.txtName.setText(this.vehicle.name);
        this.txtAvailable.setText(String.valueOf(this.vehicle.available));
        this.txtPrice.setText(String.valueOf(this.vehicle.dailyPrice));
        this.txtAgeMin.setText(String.valueOf(this.vehicle.ageMin));
        this.txtDiscount.setText(String.valueOf(this.vehicle.sale));
        this.txtCategory.setText(String.format("%c", this.vehicle.eco2Category));
        
        final VehicleDAO vehicleDAO = ApplicationDatabaseHelper.getDatabase(this.getContext()).vehicleDAO();
        boolean addedToFavorites = vehicleDAO.query(this.vehicle.id) != null;
        
        if (addedToFavorites) {
            this.imgBtnFavorite.setImageResource(android.R.drawable.star_big_on);
        }
        
        this.imgBtnFavorite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                boolean addedToFavorites = vehicleDAO.query(VehicleDetailFragment.this.vehicle.id) != null;
                
                if (addedToFavorites) {
                    VehicleDetailFragment.this.imgBtnFavorite.setImageResource(android.R.drawable.star_big_off);
                    vehicleDAO.delete(VehicleDetailFragment.this.vehicle);
                } else {
                    VehicleDetailFragment.this.imgBtnFavorite.setImageResource(android.R.drawable.star_big_on);
                    vehicleDAO.insert(VehicleDetailFragment.this.vehicle);
                }
             }
        });
        
        this.recyclerEquipments = view.findViewById(R.id.recycler_equipments);
        this.recyclerOptions = view.findViewById(R.id.recycler_options);
        
        this.recyclerEquipments.setLayoutManager(new LinearLayoutManager(this.getContext()));
        this.recyclerOptions.setLayoutManager(new LinearLayoutManager(this.getContext()));
        
        this.recyclerEquipments.setAdapter(new EquipmentListAdapter(this.vehicle.equipments));
        this.recyclerOptions.setAdapter(new OptionListAdapter(this.vehicle.options));
    }
    
}

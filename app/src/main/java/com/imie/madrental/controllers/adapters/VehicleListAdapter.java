/*
 * EventSessionListAdapter.java, presence Android
 *
 * Copyright © 2019-2020 Mickael Gaillard / TACTfactory
 * License    : all rights reserved
 */
package com.imie.madrental.controllers.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.imie.madrental.R;
import com.imie.madrental.data.DTO.VehicleDTO;
import com.imie.madrental.data.mappers.VehicleMapper;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

/**
 * List adapter for Vehicles.
 */
public class VehicleListAdapter extends RecyclerView.Adapter<VehicleListAdapter.VehicleViewHolder> {

    /**
     * Stores the list of vehicle to display.
     */
    private List<VehicleDTO> vehicles = new ArrayList<>();

    /**
     * Stores the associated listener.
     */
    private VehicleListAdapterListener listener;
    
    public VehicleListAdapter(VehicleListAdapterListener listener) {
        this.listener = listener;
    }

    /**
     * Gets the vehicles attribute.
     * @return The java.util.List<com.imie.madrental.data.DTO.VehicleDTO> value of the vehicles attribute.
     */
    public List<VehicleDTO> getVehicles() {
        return this.vehicles;
    }

    /**
     * Sets the vehicles attribute.
     * @param newVehicles The new java.util.List<com.imie.madrental.data.DTO.VehicleDTO> value to set.
     */
    public void setVehicles(List<VehicleDTO> newVehicles) {
        this.vehicles = newVehicles;
    }

    /**
     * Gets the adapterListener attribute.
     * @return The com.imie.madrental.controllers.adapters.VehicleListAdapter.VehicleListAdapterListener value of the adapterListener attribute.
     */
    public VehicleListAdapterListener getAdapterListener() {
        return this.listener;
    }

    /**
     * Sets the adapterListener attribute.
     * @param newAdapterListener The new com.imie.madrental.controllers.adapters.VehicleListAdapter.VehicleListAdapterListener value to set.
     */
    public void setAdapterListener(VehicleListAdapterListener newAdapterListener) {
        this.listener = newAdapterListener;
    }

    /**
     * Called when RecyclerView needs a new {@link ViewHolder} of the given type to represent
     * an item.
     * <p>
     * This new ViewHolder should be constructed with a new View that can represent the items
     * of the given type. You can either create a new View manually or inflate it from an XML
     * layout file.
     * <p>
     * The new ViewHolder will be used to display items of the adapter using
     * {@link #onBindViewHolder(ViewHolder, int, List)}. Since it will be re-used to display
     * different items in the data set, it is a good idea to cache references to sub views of
     * the View to avoid unnecessary {@link View#findViewById(int)} calls.
     * @param parent The ViewGroup into which the new View will be added after it is bound to
     * an adapter position.
     * @param viewType The view type of the new View.
     * @return A new ViewHolder that holds a View of the given view type.
     * @see #getItemViewType(int)
     * @see #onBindViewHolder(ViewHolder, int)
     */
    @NonNull
    @Override
    public VehicleViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.vehicle_row, parent, false);
        
        return new VehicleViewHolder(view);
    }

    /**
     * Called by RecyclerView to display the data at the specified position. This method should
     * update the contents of the {@link ViewHolder#itemView} to reflect the item at the given
     * position.
     * <p>
     * Note that unlike {@link ListView}, RecyclerView will not call this method
     * again if the position of the item changes in the data set unless the item itself is
     * invalidated or the new position cannot be determined. For this reason, you should only
     * use the <code>position</code> parameter while acquiring the related data item inside
     * this method and should not keep a copy of it. If you need the position of an item later
     * on (e.g. in a click listener), use {@link ViewHolder#getAdapterPosition()} which will
     * have the updated adapter position.
     * <p>
     * Override {@link #onBindViewHolder(ViewHolder, int, List)} instead if Adapter can
     * handle efficient partial bind.
     * @param holder The ViewHolder which should be updated to represent the contents of the
     * item at the given position in the data set.
     * @param position The position of the item within the adapter's data set.
     */
    @Override
    public void onBindViewHolder(@NonNull VehicleViewHolder holder, int position) {
        holder.populate(this.vehicles.get(position));
    }

    /**
     * Returns the total number of items in the data set held by the adapter.
     * @return The total number of items in this adapter.
     */
    @Override
    public int getItemCount() {
        return this.vehicles.size();
    }

    /**
     * Using holder pattern in order to display elements.
     */
    protected class VehicleViewHolder extends RecyclerView.ViewHolder {

        /**
         * Stores the content view.
         */
        private final View contentView;

        /**
         * Stores the vehicle name text.
         */
        private final TextView txtName;

        /**
         * Stores the vehicle price text.
         */
        private final TextView txtPrice;

        /**
         * Stores the vehicle category text.
         */
        private final TextView txtCategory;

        /**
         * Stores the vehicle image.
         */
        private final ImageView imgVehicle;

        /**
         * Constructor.
         * @param view The view.
         */
        protected VehicleViewHolder(View view) {
            super(view);

            this.contentView = view.findViewById(R.id.constraint_content);
            this.txtName = view.findViewById(R.id.txt_name);
            this.txtPrice = view.findViewById(R.id.txt_price);
            this.txtCategory = view.findViewById(R.id.txt_category);
            this.imgVehicle = view.findViewById(R.id.img_vehicle);
        }

        /**
         * Populate row with a Vehicle.
         * @param model The model to display.
         */
        protected void populate(final VehicleDTO model) {
            this.txtName.setText(model.name);
            this.txtPrice.setText(String.format("%d $ / day", model.dailyPrice.intValue()));
            this.txtCategory.setText(String.format("CO2 category: %c", model.eco2Category));
            
            Picasso.get().load(String.format(VehicleMapper.VEHICLE_IMAGE_URL, model.image)).into(this.imgVehicle);
            
            this.contentView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (VehicleListAdapter.this.listener != null) {
                        VehicleListAdapter.this.listener.vehicleSelected(model);
                    }
                }
            });
        }
    }

    /**
     * Interface used to notify when a vehicle is selected.
     */
    public interface VehicleListAdapterListener {

        /**
         * Called when a vehicle is selected.
         * @param vehicle The selected vehicle.
         */
        void vehicleSelected(VehicleDTO vehicle);
        
    }
    
}

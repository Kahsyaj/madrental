package com.imie.madrental.controllers;

import android.content.res.Configuration;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.Switch;

import com.imie.madrental.R;
import com.imie.madrental.controllers.adapters.VehicleListAdapter;
import com.imie.madrental.data.ApplicationDatabaseHelper;
import com.imie.madrental.data.DTO.VehicleDTO;
import com.imie.madrental.data.webServices.VehicleWebService;

import java.util.List;

/**
 * Fragment displaying the list of vehicles for rental.
 */
public class VehicleListFragment extends Fragment implements VehicleWebService.VehicleWebServiceListener,
        VehicleListAdapter.VehicleListAdapterListener {

    /**
     * Stores the associated tag.
     */
    public static final String TAG = "VehicleListFragment";

    /**
     * Defines the max width to display fragments side by side.
     */
    private static final int MAX_WIDTH_FOR_SPLITTING = 600;

    /**
     * Stores the switch used to filter by favorites.
     */
    private Switch switchFavorites;

    /**
     * Stores the list of vehicles.
     */
    private RecyclerView recyclerVehicles;

    /**
     * Stores the vehicle list adapter.
     */
    private VehicleListAdapter adapter;

    /**
     * Constructor.
     */
    public VehicleListFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_vehicle_list, container, false);
        
        this.init(view);
        
        return view;
    }

    /**
     * Initializes the fragment.
     * @param view The associated view.
     */
    private void init(View view) {
        this.switchFavorites = view.findViewById(R.id.switch_favorites);
        this.recyclerVehicles = view.findViewById(R.id.recycler_vehicles);
        this.adapter = new VehicleListAdapter(this);
        
        final VehicleWebService vehicleWebService = new VehicleWebService(this);
        
        this.recyclerVehicles.setLayoutManager(new LinearLayoutManager(this.getContext()));
        this.recyclerVehicles.setAdapter(this.adapter);
        this.switchFavorites.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    VehicleListFragment.this.adapter.setVehicles(ApplicationDatabaseHelper.getDatabase(VehicleListFragment.this.getContext()).vehicleDAO().getVehicles());
                    VehicleListFragment.this.adapter.notifyDataSetChanged();
                } else {
                    vehicleWebService.getVehicles();
                }
            }
        });
        
        vehicleWebService.getVehicles();
    }

    /**
     * Called on success to notify with the vehicles obtained.
     * @param vehicles The vehicles retrieved from the API.
     */
    @Override
    public void onVehicleListSuccess(List<VehicleDTO> vehicles) {
        this.adapter.setVehicles(vehicles);
        this.adapter.notifyDataSetChanged();
    }

    /**
     * Called when a vehicle is selected.
     * @param vehicle The selected vehicle.
     */
    @Override
    public void vehicleSelected(VehicleDTO vehicle) {
        Configuration configuration = getResources().getConfiguration();
        int screenWidthDp = configuration.screenWidthDp;
        VehicleDetailFragment fragment = new VehicleDetailFragment();
        Bundle bundle = new Bundle();
        
        bundle.putSerializable(MainActivity.VEHICLE_KEY, vehicle);
        fragment.setArguments(bundle);
        
        FragmentManager fragmentManager = this.getActivity().getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE && screenWidthDp >= MAX_WIDTH_FOR_SPLITTING) {
            fragmentTransaction.replace(R.id.frame_detail, fragment, VehicleDetailFragment.TAG);
            ((MainActivity) this.getActivity()).findViewById(R.id.frame_detail).setVisibility(View.VISIBLE);
        } else {
            fragmentTransaction.replace(R.id.fragment_vehicle_list, fragment, VehicleDetailFragment.TAG);
            
            ((MainActivity) this.getActivity()).setNewFragmentShown(true);
        }
        
        fragmentTransaction.commit();
    }
}

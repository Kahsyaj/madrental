package com.imie.madrental.data.DTO;

import com.imie.madrental.data.mappers.VehicleMapper;

import java.io.Serializable;
import java.util.Map;

/**
 * Defines an option element.
 */
public class OptionDTO implements Serializable {

    /**
     * Stores the option id.
     */
    public Double id;

    /**
     * Stores the option name.
     */
    public String name;

    /**
     * Stores the option price.
     */
    public Double price;
    
    /**
     * Required empty constructor.
     */
    public OptionDTO() {}
    
    /**
     * Constructor from json data.
     * @param json The json containing the elements to initialize the option.
     */
    public OptionDTO(Map<String, Object> json) {
        this();
        
        this.id = (Double) json.get(VehicleMapper.OptionsMapper.ID);
        this.name = (String) json.get(VehicleMapper.OptionsMapper.NAME);
        this.price = (Double) json.get(VehicleMapper.OptionsMapper.PRICE);
    }
    
}

package com.imie.madrental.data.mappers;

/**
 * Mapper class used to map vehicle json values from the API.
 */
public abstract class VehicleMapper {

    /**
     * Defines the vehicle list url.
     */
    public static final String VEHICLES_URL = "http://s519716619.onlinehome.fr/exchange/madrental/get-vehicules.php";

    /**
     * Defines the vehicle image url.
     */
    public static final String VEHICLE_IMAGE_URL = "http://s519716619.onlinehome.fr/exchange/madrental/images/%s";

    /**
     * Defines the id key.
     */
    public static final String ID = "id";

    /**
     * Defines the name key.
     */
    public static final String NAME = "nom";

    /**
     * Defines the image key.
     */
    public static final String IMAGE = "image";

    /**
     * Defines the available key.
     */
    public static final String AVAILABLE = "disponible";

    /**
     * Defines the price key.
     */
    public static final String PRICE = "prixjournalierbase";

    /**
     * Defines the sale key.
     */
    public static final String SALE = "promotion";

    /**
     * Defines the minimum age key.
     */
    public static final String AGE_MIN = "agemin";

    /**
     * Defines the co2 category key.
     */
    public static final String CATEGORY = "categorieco2";

    /**
     * Defines the equipments key.
     */
    public static final String EQUIPMENTS = "equipements";

    /**
     * Inner class used to map equipments data as the elements aren't persisted.
     */
    public static class EquipmentMapper {
        
        /**
         * Defines the id key.
         */
        public static final String ID = "id";

        /**
         * Defines the id key.
         */
        public static final String NAME = "nom";
        
    }

    /**
     * Defines the equipments key.
     */
    public static final String OPTIONS = "options";

    /**
     * Inner class used to map options data as the elements aren't persisted.
     */
    public static class OptionsMapper {

        /**
         * Defines the id key.
         */
        public static final String ID = "id";

        /**
         * Defines the id key.
         */
        public static final String NAME = "nom";

        /**
         * Defines the id key.
         */
        public static final String PRICE = "prix";

    }
    
}

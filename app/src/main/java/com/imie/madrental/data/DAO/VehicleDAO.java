package com.imie.madrental.data.DAO;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.imie.madrental.data.DTO.VehicleDTO;

import java.util.List;

@Dao
public abstract class VehicleDAO {

    @Query("SELECT * FROM favorite_vehicle")
    public abstract List<VehicleDTO> getVehicles();

    @Query("SELECT * FROM favorite_vehicle WHERE id = :id")
    public abstract VehicleDTO query(Double id);
    
    @Insert
    public abstract void insert(VehicleDTO... vehicles);
    @Update
    public abstract void update(VehicleDTO... vehicles);
    @Delete
    public abstract void delete(VehicleDTO... vehicles);
    
}

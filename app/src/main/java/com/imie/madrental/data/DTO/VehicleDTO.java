package com.imie.madrental.data.DTO;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import com.imie.madrental.data.mappers.VehicleMapper;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * DTO class used to represent vehicle entities.
 */
@Entity(tableName = "favorite_vehicle")
public class VehicleDTO implements Serializable {

    /**
     * Stores the vehicle id.
     */
    @PrimaryKey
    @NonNull
    public Double id;

    /**
     * Stores the vehicle name.
     */
    public String name;

    /**
     * Stores the vehicle image url.
     */
    public String image;

    /**
     * Defines if the vehicle is available for rental.
     */
    public boolean available;

    /**
     * Defines the daily price to rent the vehicle.
     */
    public Double dailyPrice;

    /**
     * Defines the current sale for the rental.
     */
    public Double sale;

    /**
     * Minimum age.
     */
    public Double ageMin;

    /**
     * Defines the eco 2 category.
     */
    public char eco2Category;

    /**
     * Stores the vehicle's equipments.
     */
    @Ignore
    public List<EquipmentDTO> equipments = new ArrayList<>();

    /**
     * Stores the vehicle options.
     */
    @Ignore
    public List<OptionDTO> options = new ArrayList<>();

    /**
     * Required empty constructor.
     */
    public VehicleDTO() {}
    
    /**
     * Constructor from json data.
     * @param json The json containing the elements to initialize the vehicle.
     */
    public VehicleDTO(Map<String, Object> json) {
        this();
        
        this.id = (Double) json.get(VehicleMapper.ID);
        this.name = (String) json.get(VehicleMapper.NAME);
        this.image = (String) json.get(VehicleMapper.IMAGE);
        this.available = ((Double) json.get(VehicleMapper.AVAILABLE)) == 1;
        this.dailyPrice = (Double) json.get(VehicleMapper.PRICE);
        this.sale = (Double) json.get(VehicleMapper.SALE);
        this.ageMin = (Double) json.get(VehicleMapper.AGE_MIN);
        this.eco2Category = ((String) json.get(VehicleMapper.CATEGORY)).charAt(0);
        
        for (Map<String, Object> elt : (List<Map<String, Object>>) json.get(VehicleMapper.EQUIPMENTS)) {
            this.equipments.add(new EquipmentDTO(elt));
        }

        for (Map<String, Object> elt : (List<Map<String, Object>>) json.get(VehicleMapper.OPTIONS)) {
            this.options.add(new OptionDTO(elt));
        }
    }
    
}

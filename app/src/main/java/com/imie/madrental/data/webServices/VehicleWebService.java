package com.imie.madrental.data.webServices;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.internal.LinkedTreeMap;
import com.imie.madrental.data.DTO.VehicleDTO;
import com.imie.madrental.data.mappers.VehicleMapper;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.Header;

/**
 * WebService class used to get vehicles from the API.
 */
public class VehicleWebService {

    /**
     * Defines the tag.
     */
    private static final String TAG = "VehicleWebService";

    /**
     * Stores the associated client.
     */
    private final AsyncHttpClient client = new AsyncHttpClient();

    /**
     * Stores the associated listener in order to notify on response.
     */
    private final VehicleWebServiceListener listener;

    /**
     * Constructor
     * @param listener The listener to set.
     */
    public VehicleWebService(VehicleWebServiceListener listener) {
        this.listener = listener;
    }
    
    public void getVehicles() {
        this.client.get(VehicleMapper.VEHICLES_URL, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                String jsonResponse = new String(responseBody);
                Gson gson = new Gson();
                ArrayList<LinkedTreeMap<String, Object>> elts = gson.fromJson(jsonResponse, ArrayList.class);
                ArrayList<VehicleDTO> vehicles = new ArrayList<>();
                
                for (LinkedTreeMap<String, Object> elt : elts) {
                    vehicles.add(new VehicleDTO(elt));
                }
                
                if (VehicleWebService.this.listener != null) {
                    VehicleWebService.this.listener.onVehicleListSuccess(vehicles);
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                Log.e(TAG, error.toString());
            }
        });
    }
    
    

    /**
     * Listener interface used to notify on response from API.
     */
    public interface VehicleWebServiceListener {

        /**
         * Called on success to notify with the vehicles obtained.
         * @param vehicles The vehicles retrieved from the API.
         */
        void onVehicleListSuccess(List<VehicleDTO> vehicles);
    }
    
}

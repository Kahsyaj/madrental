package com.imie.madrental.data.DTO;

import com.imie.madrental.data.mappers.VehicleMapper;

import java.io.Serializable;
import java.util.Map;

/**
 * Defines an equipment element.
 */
public class EquipmentDTO implements Serializable {

    /**
     * Stores the equipment id.
     */
    public Double id;

    /**
     * Stores the equipment name.
     */
    public String name;
    
    /**
     * Required empty constructor.
     */
    public EquipmentDTO() {}
    
    /**
     * Constructor from json data.
     * @param json The json containing the elements to initialize the equipment.
     */
    public EquipmentDTO(Map<String, Object> json) {
        this();
        
        this.id = (Double) json.get(VehicleMapper.EquipmentMapper.ID);
        this.name = (String) json.get(VehicleMapper.EquipmentMapper.NAME);
    }
    
}

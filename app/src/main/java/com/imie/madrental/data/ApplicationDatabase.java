package com.imie.madrental.data;

import androidx.room.Database;
import androidx.room.RoomDatabase;

import com.imie.madrental.data.DTO.VehicleDTO;
import com.imie.madrental.data.DAO.VehicleDAO;

/**
 * Application database class.
 */
@Database(entities = {VehicleDTO.class}, version = 1)
public abstract class ApplicationDatabase extends RoomDatabase {

    /**
     * Stores the vehicle DAO.
     * @return The associated DAO.
     */
    public abstract VehicleDAO vehicleDAO();
    
}

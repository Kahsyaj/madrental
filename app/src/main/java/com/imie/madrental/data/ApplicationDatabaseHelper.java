package com.imie.madrental.data;

import android.content.Context;

import androidx.room.Room;

/**
 * Database helper class used to get and hold the application database.
 */
public class ApplicationDatabaseHelper {

    /**
     * Defines the database name.
     */
    private static final String DB_NAME = "course.db";

    /**
     * Stores the instance of the helper (singleton pattern).
     */
    private static ApplicationDatabaseHelper helper = null;

    /**
     * Stores the instance of the application database.
     */
    private ApplicationDatabase database;

    /**
     * Private constructor in order to prevent from instanciating.
     * @param ctx The required context.
     */
    private ApplicationDatabaseHelper(Context ctx) {
        this.database = Room.databaseBuilder(ctx, ApplicationDatabase.class, DB_NAME).allowMainThreadQueries().build();
    }
    
    public static synchronized ApplicationDatabase getDatabase(Context ctx) {
        if (helper == null) {
            helper = new ApplicationDatabaseHelper(ctx);
        }
        
        return helper.database;
    }
    
}
